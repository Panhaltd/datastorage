package com.example.datastorage;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.example.datastorage.entities.User;
import com.example.datastorage.room.AppDatabase;

import java.util.List;

public class testRoom extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_room);

        AppDatabase db = Room.databaseBuilder(this,
                AppDatabase.class,"sc_db")
                .allowMainThreadQueries()
                .build();

        db.getUserDao().insert(new User("KSHRD","12345"));
        List<User> userList = db.getUserDao().findAll();
        Log.d("tag",userList.toString());
    }
}
