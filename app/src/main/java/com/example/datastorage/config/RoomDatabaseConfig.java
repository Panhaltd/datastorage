package com.example.datastorage.config;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.datastorage.UriConverter;
import com.example.datastorage.dao.MDao;
import com.example.datastorage.dao.UserDao;
import com.example.datastorage.entities.Movie;
import com.example.datastorage.entities.User;

@Database(entities = {Movie.class, User.class},version = 1)
@TypeConverters({UriConverter.class})
public abstract class RoomDatabaseConfig extends RoomDatabase {

    //Create Dao Method
    public abstract MDao movieDao();
    public abstract UserDao userDao();

    //Create Singleton OBJ;
    public static RoomDatabaseConfig INSTANCE;

    //Initializa Singleton object;
    public static RoomDatabaseConfig getRoomDB(Context context){
        if (INSTANCE==null)
        {
            INSTANCE= Room.databaseBuilder(context.getApplicationContext(),
                    RoomDatabaseConfig.class,
                    "db_movie").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }
    //For Destroy singleTon
    public static void destroy()
    {
        INSTANCE=null;
    }
}
