package com.example.datastorage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private Button btnGetData;
    String message = "I love you Bormey,Monineath,YuLing,Kannitha,and Kimbouy Only!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);

        SharedPreferences.Editor editor = preferences.edit();

        //Manipulate data

        editor.putString("secret","12345");
        editor.apply();

        textView=findViewById(R.id.data_text);
        btnGetData=findViewById(R.id.btn_get_data);

        if(preferences.contains("secret"))
        {
            String secret = preferences.getString("secret","No Value");
            textView.setText(secret);
        }else {
            textView.setText("Data Removed!");
        }
        btnGetData.setOnClickListener(v->{
            write();
//            if (preferences.contains("secret"))
//            {
//                editor.remove("secret");
//                editor.apply();
//            }
        });
        read();

    }
    void write(){
        try {
            OutputStreamWriter outputStream = new OutputStreamWriter(openFileOutput("internal",MODE_PRIVATE));
            outputStream.write(message);
            outputStream.close();
            Toast.makeText(this,"Wrote Succeed",Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void read(){
        try {
            FileInputStream fileInputStream = openFileInput("internal");
            InputStreamReader inputStream = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStream);
            String msg=bufferedReader.readLine();
            Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
