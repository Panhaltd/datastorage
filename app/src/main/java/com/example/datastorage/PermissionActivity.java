package com.example.datastorage;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

public class PermissionActivity extends AppCompatActivity {

    final int REQUEST_CODE = 112;
    String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        if (ActivityCompat.checkSelfPermission(this,permission[0])==PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(this,"Granted",Toast.LENGTH_LONG);
        }else {
            Toast.makeText(this,"Not Yet Granted",Toast.LENGTH_LONG);
            ActivityCompat.requestPermissions(this,permission,REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        String msg = grantResults+"";
        if (requestCode==REQUEST_CODE)
        {
            if (grantResults.length>0&&grantResults[0]== PackageManager.PERMISSION_GRANTED)
            Toast.makeText(this,"Allowed",Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this,"Denied",Toast.LENGTH_LONG).show();
        }
    }
}
