package com.example.datastorage.UI;

import android.content.*;
import android.view.*;
import android.widget.*;
import android.view.LayoutInflater;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.datastorage.AddMovieActivity;
import com.example.datastorage.R;
import com.example.datastorage.config.RoomDatabaseConfig;
import com.example.datastorage.entities.Movie;

import java.util.*;

public class MovieRecyclerAdapter extends RecyclerView.Adapter<MovieRecyclerAdapter.MovieViewHolder> {

    private List<Movie> movieDataset;
    private Context context;
    public MovieRecyclerAdapter( List<Movie> movieDataset, Context context){
        this.movieDataset=movieDataset;
        this.context=context;
    }

    @NonNull
    @Override
    public MovieRecyclerAdapter.MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item,parent,false);
        context = parent.getContext();
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieRecyclerAdapter.MovieViewHolder holder, int position) {
        holder.textTitle.setText(movieDataset.get(position).getTitle());
        holder.textDesc.setText(movieDataset.get(position).getDescription());
//        Glide.with(context).load(movieDataset.get(position).getThumbnail()).into(holder.imageMovie);
        holder.imageMovie.setImageURI(movieDataset.get(position).getThumbnail());
    }

    @Override
    public int getItemCount() {
        return movieDataset.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder{
        ImageView imageMovie,imageMore;
        TextView textTitle,textDesc;
        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            imageMovie=itemView.findViewById(R.id.imageView);
            textTitle=itemView.findViewById(R.id.txt_title);
            textDesc=itemView.findViewById(R.id.txt_des);
            imageMore=itemView.findViewById(R.id.imageMore);
            imageMore.setOnClickListener(v -> {
                PopupMenu popupMenu = new PopupMenu(context,imageMore);
                popupMenu.getMenuInflater().inflate(R.menu.option_more,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.opt_delete:
                            {
                                RoomDatabaseConfig.getRoomDB(context).movieDao().deleteOne(movieDataset.get(getAdapterPosition()));
                            }
                            case R.id.opt_edit:{
                                Movie movie = new Movie(
                                        movieDataset.get(getAdapterPosition()).getTitle(),
                                        movieDataset.get(getAdapterPosition()).getDescription(),
                                        null
                                );
                                movie.setId(movieDataset.get(getAdapterPosition()).getId());
                                Intent updateIntent = new Intent(context, AddMovieActivity.class);
                                updateIntent.putExtra("update",true);
                                updateIntent.putExtra("editMovie",movie);
                                context.startActivity(updateIntent);
                                RoomDatabaseConfig.getRoomDB(context).movieDao().updateOne(movieDataset.get(getAdapterPosition()));
                            }
                        }
                        return true;
                    }
                });
                popupMenu.show();
            });
        }
    }
}
