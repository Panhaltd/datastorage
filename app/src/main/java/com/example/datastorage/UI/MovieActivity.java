package com.example.datastorage.UI;

;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.datastorage.AddMovieActivity;
import com.example.datastorage.ImageURL;
import com.example.datastorage.R;
import com.example.datastorage.config.RoomDatabaseConfig;
import com.example.datastorage.entities.Movie;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.*;

public class MovieActivity extends AppCompatActivity {

    private List<Movie> movieDataset;
    private MovieRecyclerAdapter movieAdapter;
    private LinearLayoutManager layoutManager;
    private RecyclerView recyclerView;
    private FloatingActionButton fabAdd;
    private final static int READ_INTERNAL_CODE=100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        fabAdd=findViewById(R.id.ftb_more);
        fabAdd.setOnClickListener(v -> {
            Intent addIntent = new Intent(MovieActivity.this, AddMovieActivity.class);
            startActivity(addIntent);
        });
//        RoomDatabaseConfig.getRoomDB(this).movieDao().removeAll();

//        RoomDatabaseConfig.getRoomDB(this).movieDao().insert(new Movie("Avengers","Avergers!Assemble!", ImageURL.AVENGERS),
//                new Movie("Frozen 2","Frozen in the heart",ImageURL.FROZEN_2),
//                new Movie("Joker","The World of Fool",ImageURL.JOKER));

        recyclerView=findViewById(R.id.rec_movie);
        getMovie();
        if (!checkPermission()){
            requestPermission();
        }
    }

    private void getMovie()
    {
        movieDataset=new ArrayList<>();
        movieDataset=RoomDatabaseConfig.getRoomDB(this).movieDao().selectAll();
        layoutManager=new LinearLayoutManager(this);
        movieAdapter=new MovieRecyclerAdapter(movieDataset,this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(movieAdapter);
        movieAdapter.notifyDataSetChanged();
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                READ_INTERNAL_CODE);
    }
    private boolean checkPermission(){
        if(ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }else {
            return false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        movieDataset=null;
        getMovie();
    }
}
