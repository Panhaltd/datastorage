package com.example.datastorage;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.datastorage.config.RoomDatabaseConfig;
import com.example.datastorage.entities.Movie;

public class AddMovieActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnChoose;
    private Button btnAdd;
    private EditText etTitle;
    private EditText etDesc;
    private Uri imageUri;
    private int CHOOSE_IMAGE = 1;
    private int updateID;
    private ImageView imagePreview;

    private void initView()
    {
        btnAdd=findViewById(R.id.btn_addMovie);
        btnChoose=findViewById(R.id.btn_choose);
        etTitle=findViewById(R.id.et_title);
        etDesc=findViewById(R.id.et_desc);
        imagePreview=findViewById(R.id.imagePreview);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);
        initView();
        btnChoose.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        onNewIntent(getIntent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_choose:{
                Intent imageIntent = new Intent(Intent.ACTION_PICK);
                imageIntent.setType("image/*");
                startActivityForResult(imageIntent,CHOOSE_IMAGE);
            }break;
            case R.id.btn_addMovie:{
                if(btnAdd.getText()=="Update"){
                    String title = etTitle.getText().toString();
                    String desc = etDesc.getText().toString();
                    Uri thumbnail = imageUri;
                    Movie movie = new Movie(title, desc, thumbnail);
                    movie.setId(updateID);
                    RoomDatabaseConfig.getRoomDB(this).movieDao().updateOne(movie);
                    finish();
                }else {
                    String title = etTitle.getText().toString();
                    String desc = etDesc.getText().toString();
                    Uri thumbnail = imageUri;
                    Movie movie = new Movie(title, desc, thumbnail);
                    RoomDatabaseConfig.getRoomDB(this).movieDao().insert(movie);
                    finish();
                }break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==CHOOSE_IMAGE
                &&resultCode==RESULT_OK
                &&data!=null){
            imageUri = data.getData();
            imagePreview.setImageURI(imageUri);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if(extras!=null){
            if (extras.getBoolean("update"));{
                Movie movie = (Movie)extras.getSerializable("editMovie");
                updateID=movie.getId();
                btnAdd.setText("Update");
                etTitle.setText(movie.getTitle());
                etDesc.setText(movie.getDescription());
            }
        }
    }
}
