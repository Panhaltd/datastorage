package com.example.datastorage.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.datastorage.entities.Movie;

import java.util.List;

@Dao
public interface MDao {
    @Insert
    void insert(Movie...movie);

    @Query("SELECT * FROM movie")
    List<Movie> selectAll();

    @Delete
    void deleteOne(Movie movie);

    @Update
    void updateOne(Movie movie);
}
